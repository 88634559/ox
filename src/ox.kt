fun main() {
    Game()
}

private fun Game() {
    showWelcome()
    while (true) {
        try {
            print("Please input row, col : ")
            val input = readLine()
            val numStr = input?.split(" ")
            if (numStr?.size ?: 0 == 2) {
                val row = numStr?.get(0)?.toInt()
                val col = numStr?.get(1)?.toInt()
                if (table[row!!][col!!] == '-') {
                    table[row!!][col!!] = currentPlayerMark
                    count++
                    printTable()
                    if (checkRowCol()) break
                    showTurn()
                    if (stop()) break
                } else {
                    println("Please input pow, col!!!")
                    printTable()
                }
            } else {
                println("Please input row, col!!!")
                printTable()
            }
        } catch (e: NumberFormatException) {
            println("Please input row, col in number format!!!")
            printTable()
        }
    }
}

private fun stop(): Boolean {
    if (count == 9) {
        return true
    }
    return false
}

private fun checkRowsForWin(): Boolean {
    for (i in 0..2) {
        if (table[i][0] == currentPlayerMark && table[i][1] == currentPlayerMark && table[i][2] == currentPlayerMark) {
            return true
        }
    }
    return false
}

private fun checkColumnsForWin(): Boolean {
    for (i in 0..2) {
        if (table[0][i] == currentPlayerMark && table[1][i] == currentPlayerMark && table[2][i] == currentPlayerMark) {
            return true
        }
    }
    return false
}

private fun checkDiagonalsForWin(): Boolean {
    for (i in 0..2) {
        if (table[0][0] == currentPlayerMark && table[1][1] == currentPlayerMark && table[2][2] == currentPlayerMark) {
            return true
        }
    }
    return false
}

val table = arrayOf(charArrayOf('-', '-', '-'),
    charArrayOf('-', '-', '-'),
    charArrayOf('-', '-', '-'))

var currentPlayerMark = 'x'
private fun showTurn() {
    if (currentPlayerMark == 'x') {
        currentPlayerMark = 'o'
        println("o Turn")
    } else {
        currentPlayerMark = 'x'
        println("x Turn")
    }
}

var count = 0

private fun checkRowCol(): Boolean {
    if (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin()) {
        if (currentPlayerMark == 'x') {
            println("x Win")
            return true
        }
        if (currentPlayerMark == 'y') {
            println("y Win")
            return true
        }
    }
    if (!checkRowsForWin() && !checkColumnsForWin() && !checkDiagonalsForWin() && count == 9) {
        println("Down")
        return true
    }
    return false
}

private fun showWelcome() {
    println("Welcome to OX Game")
    printTable()
    println("x Turn")
}

private fun printTable() {
    for (row in table) {
        for (col in row) {
            print(col)
        }
        println()
    }
}